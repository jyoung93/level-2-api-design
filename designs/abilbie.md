# _UniLincoln Memories_ API Design

Host: `api.unilincolnmemories.co.uk`
Version: `1`

---

### Endpoint: `/memories`

#### Parameters

* `user_id` (optional; type: int) Filter memories by a particular user
* `limit` (optional; type: int) The number of memories to return
* `page` (optional; type: int) Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results

#### Example Response

Response code: `200`

```json
{
	"memories": [
		{
			"user_id": 234,
			"location": [53.228833,-0.549236],
			"memory_type": "text",
			"memory_tags": ["lecture", "funny"],
			"memory_text": "I'll always remmeber the day Kevin told us about his cat Marmalade!"
		},
		{
			"user_id": 349,
			"location": [53.229262,-0.548844],
			"memory_type": "photo",
			"memory_tags": ["The Shed", "nightlife"],
			"memory_text": "I'll always remmeber the day Kevin told us about his cat Marmalade!",
			"memory_photo_url": "http://1.bp.blogspot.com/-ZG-ki2PQKEk/ULtrrNBRxCI/AAAAAAAAHOo/0rQhjCMG034/s1600/too.jpg"
		},
	]
}
```

