# UniLincoln Memories

_UniLincoln Memories_ is a hypothetical website that allows final year students from the University of Lincoln to sign in and share their memories. Students sign in with their university credentials and then they can upload photos, videos or write up their memories. Each “memory” can have tags associated with them, and can be associated with a geographical location.

The company behind _UniLincoln Memories_ have had many requests for them to make mobile applications but in order to do this they first have to design and build an API which can be used by the software developers they’ve contracted to build the apps.

## Your Task

Your task is to design a RESTful API for _UniLincoln Memories_.

Think about the service described above; what data is being dealt with? What 3rd party services are being used? What API endpoints are required in order to emulate the functionality of the web site.

You should consider:

* What is a suitable data response type for mobile applications?
* HTTP verbs
* If you’re uploading binary data, are additional HTTP headers required?
* How is the API secured?

Some places to look for inspiration:

* Stack Overflow
* Facebook/Twitter/Google developer documentation
* Google

## How to submit your API designs

1. Fork this repository
2. Take a look at `designs/abilbie.md` to see an example
3. Create your own file in the `designs` folder with the filename `{your student number}.md`

If you need some help with Markdown format take a look at [http://daringfireball.net/projects/markdown/syntax](http://daringfireball.net/projects/markdown/syntax)